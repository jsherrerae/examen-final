import math 

GRAVEDAD=9.8 
def funcion1(angulo,coRozamiento,masa1):
  masa2=0 
  angulo=math.radians(angulo) 
  seno=math.sin(angulo)
  coseno=math.cos(angulo)
  resultado=[]
  while True:
    aceleracion=GRAVEDAD*(masa1*seno-masa2-coRozamiento*masa1*coseno)/(masa1+masa2)
    if aceleracion<0:
      resultado.append([masa2,"cae"])
      break
    else:
      resultado.append([masa2,"sube"]) 
      masa2+=0.5
  return resultado
      
print(funcion1(30,0.2,16))

def funcion2(masa1,masa2,coRozamiento):
  for angulo in range(10,86):
    angulo=math.radians(angulo)
    seno=math.sin(angulo)
    coseno=math.cos(angulo)
    aceleracion=GRAVEDAD*(masa1*seno-masa2-coRozamiento*masa1*coseno)/(masa1+masa2)
    
    aceleracion=round(aceleracion,1)
    
    if aceleracion==0:
      return math.degrees(angulo)
  return -1

print(funcion2(7,4,0.2))